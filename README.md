# docker-compose-application

This template docker-compose configuration reflects the standard setup at
Naturalis for web services deployed with docker-compose.

Use
[ansible-role-docker-compose](https://gitlab.com/naturalis/lib/ansible/ansible-role-docker-compose)
to start and manage your docker-compose application using Ansible.

## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

- `pre-commit autoupdate`
- `pre-commit install`

